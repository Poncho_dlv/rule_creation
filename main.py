import json
import sys
import sqlite3
import re
from spike_database.TCSkills import TCSkills
from spike_database.TCRaces import TCRaces
from spike_database.TCPlayers import TCPlayers
from spike_database.TCStarPlayers import TCStarPlayers
from spike_database.TCRules import TCRules
from spike_database.TCInjuries import TCInjuries
from spike_database.TCInducements import TCInducements

skill_type = {
    "1": "general",
    "2": "agility",
    "3": "passing",
    "4": "strength",
    "5": "mutation",
    "6": "extraordinary",
    "7": "stats_increase",
    "8": "casualty"
}


def get_races():
    conn = sqlite3.connect("spike_resources/databases/Rules.db")
    cursor = conn.cursor()
    cursor.execute("""SELECT ID, DataConstant, RerollPrice FROM Races WHERE IsTeam==1""")
    response = cursor.fetchall()
    conn.close()
    return response


def get_players(race_id):
    conn = sqlite3.connect("spike_resources/databases/Rules.db")
    cursor = conn.cursor()
    cursor.execute("""SELECT ID, DataConstant, IdRaces, CharacsMovementAllowance,  CharacsStrength, CharacsAgility, CharacsArmourValue, Price, MaxQuantity FROM PlayerTypes WHERE Idraces=?""", (race_id,))
    response = cursor.fetchall()
    conn.close()
    return response


def get_player(player_id):
    conn = sqlite3.connect("spike_resources/databases/Rules.db")
    cursor = conn.cursor()
    cursor.execute("""SELECT ID, DataConstant, IdRaces, CharacsMovementAllowance,  CharacsStrength, CharacsAgility, CharacsArmourValue, Price, MaxQuantity FROM PlayerTypes WHERE ID=?""", (player_id,))
    response = cursor.fetchone()
    conn.close()
    return response


def get_player_skills(player_id):
    conn = sqlite3.connect("spike_resources/databases/Rules.db")
    cursor = conn.cursor()
    cursor.execute("""SELECT IdSkillListing FROM PlayerTypeSkills WHERE IdPlayerTypes=?""", (player_id,))
    response = cursor.fetchall()
    conn.close()
    return response


def get_skill_listing():
    conn = sqlite3.connect("spike_resources/databases/Rules.db")
    cursor = conn.cursor()
    cursor.execute("""SELECT ID, DataConstant, IdSkillCategories FROM SkillListing""")
    response = cursor.fetchall()
    conn.close()
    return response


def get_simple_skill_type(player_id):
    conn = sqlite3.connect("spike_resources/databases/Rules.db")
    cursor = conn.cursor()
    cursor.execute("""SELECT IdSkillCategories FROM PlayerTypeSkillCategoriesNormal WHERE IdPlayerTypes=?""", (player_id,))
    response = cursor.fetchall()
    conn.close()
    return response


def get_double_skill_type(player_id):
    conn = sqlite3.connect("spike_resources/databases/Rules.db")
    cursor = conn.cursor()
    cursor.execute("""SELECT IdSkillCategories FROM PlayerTypeSkillCategoriesDouble WHERE IdPlayerTypes=?""", (player_id,))
    response = cursor.fetchall()
    conn.close()
    return response


def get_star_player():
    conn = sqlite3.connect("spike_resources/databases/Rules.db")
    cursor = conn.cursor()
    cursor.execute("""SELECT IdRaces, IdPlayerTypes, Description  FROM RacesStarPlayers""")
    response = cursor.fetchall()
    conn.close()
    return response


def read_json(filename):
    try:
        with open(filename, encoding="utf-8") as f:
            data = json.load(f)
    except:
        data = {}
    return data


def generate_skills():
    skill_description = read_json("skills_descriptions.json")
    bb2_skills = get_skill_listing()
    skill_db = TCSkills()

    merged_data = {}

    for skill in bb2_skills:
        skill_data = {}
        if skill[2] < 7:
            data = next((item for item in skill_description if item["name"].replace(" ", "").replace("&", "") == skill[1]), None)
            if data is not None:
                skill_data = {
                    "name": data["name"],
                    "description": data["description"],
                    "type": skill_type[str(skill[2])],
                    "img": "https://spike.ovh/static/spike_resources/img/skillicons/{}.png".format(skill[1])
                }
        else:
            display_name = skill[1].replace("Increase", "Increase ")
            stat = skill[1].replace("Increase", "")
            skill_data = {
                "name": display_name,
                "description": "Increase the {} of the player by one".format(stat.lower()),
                "type": skill_type[str(skill[2])],
                "img": "https://spike.ovh/static/spike_resources/img/skillicons/{}.png".format(skill[1])
            }
        skill_id = skill_db.add_skill(skill_data)
        merged_data[str(skill[0])] = skill_id
    return merged_data


def generate_races(players_id, star_players_ids, inducements_ids):
    bb2_races = get_races()
    races_db = TCRaces()
    merged_data = {}

    for race in bb2_races:
        players_list = get_players(race[0])
        roster = []
        star_players = []

        for player in players_list:
            roster.append(players_id[str(player[0])])

        for star_player in star_players_ids.values():
            if race[0] in star_player["races"]:
                star_players.append(star_player["id"])

        name = re.sub("([a-z])([A-Z])", "\g<1> \g<2>", race[1])
        race_data = {
            "name": name,
            "img": "https://spike.ovh/static/spike_resources/img/pictos/{}.png".format(race[1]),
            "reroll_price": int(int(race[2])/1000),
            "star_players": star_players,
            "roster": roster,
            "inducements": inducements_ids
        }
        race_id = races_db.add_race(race_data)
        merged_data[str(race[0])] = race_id
    return merged_data


def generate_players(skill_ids):
    bb2_races = get_races()
    player_db = TCPlayers()

    players_ids = {}

    for race in bb2_races:
        race_id = race[0]
        players = get_players(race_id)
        for player in players:
            skills = get_player_skills(player[0])
            skill_list = []
            for skill in skills:
                skill_list.append(skill_ids[str(skill[0])])

            simple_skill = []
            double_skill = []
            for x in get_simple_skill_type(player[0]):
                simple_skill.append(skill_type[str(x[0])])
            for x in get_double_skill_type(player[0]):
                double_skill.append(skill_type[str(x[0])])

            player_type = player[1].replace("{}_".format(race[1]), "")
            player_type = player_type.replace("_", " ")
            player_type = re.sub("([a-z])([A-Z])", "\g<1> \g<2>", player_type)
            player_data = {
                "player_type": player_type,
                "max_number": player[8],
                "price": int(player[7] / 1000),
                "ma": player[3],
                "st": player[4],
                "ag": player[5],
                "av": player[6],
                "starting_skills": skill_list,
                "simple_skill": simple_skill,
                "double_skill": double_skill
            }
            player_id = player_db.add_player(player_data)
            players_ids[str(player[0])] = player_id
    return players_ids


def generate_star_players(skill_ids):
    bb2_star_players = get_star_player()
    star_player_db = TCStarPlayers()
    star_players = {}

    for player in bb2_star_players:
        name = player[2]
        if "fallback" not in name.lower() and "allstars" not in name.lower():
            player_type_id = str(player[1])
            id_race = player[0]
            if star_players.get(player_type_id) is None:
                skills = get_player_skills(player_type_id)
                player = get_player(player_type_id)
                skill_list = []
                for skill in skills:
                    skill_list.append(skill_ids[str(skill[0])])

                player_type = player[1].replace("StarPlayer_", "")
                player_type = re.sub("([a-z])([A-Z])", "\g<1> \g<2>", player_type)

                star_player_data = {
                    "player_type": player_type,
                    "max_number": player[8],
                    "price": int(player[7] / 1000),
                    "ma": player[3],
                    "st": player[4],
                    "ag": player[5],
                    "av": player[6],
                    "starting_skills": skill_list
                }
                star_player_id = star_player_db.add_star_player(star_player_data)
                star_players[player_type_id] = {
                    "id": star_player_id,
                    "races": [id_race]
                }
            else:
                star_players[player_type_id]["races"].append(id_race)
    return star_players


def generate_injuries():
    injuries_db = TCInjuries()
    injuries_ids = []
    casualties = [
      {"name": "Broken jaw", "effect": "MNG", "info": "Miss next game.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_MNG.png"},
      {"name": "Broken ribs", "effect": "MNG", "info": "Miss next game.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_MNG.png"},
      {"name": "Fractured arm", "effect": "MNG", "info": "Miss next game.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_MNG.png"},
      {"name": "Fractured leg", "effect": "MNG", "info": "Miss next game.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_MNG.png"},
      {"name": "Smashed hand", "effect": "MNG", "info": "Miss next game.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_MNG.png"},
      {"name": "Gouged eye", "effect": "MNG", "info": "Miss next game.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_MNG.png"},
      {"name": "Groin strain", "effect": "MNG", "info": "Miss next game.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_MNG.png"},
      {"name": "Pinched nerve", "effect": "MNG", "info": "Miss next game.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_MNG.png"},
      {"name": "Damaged back", "effect": "NIGGLING", "info": "Niggling injury: adds 1 to all the player's injury rolls.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_Niggle.png"},
      {"name": "Smashed knee", "effect": "NIGGLING", "info": "Niggling injury: adds 1 to all the player's injury rolls.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_Niggle.png"},
      {"name": "Smashed ankle", "effect": "DEC_MA", "info": "Loses 1 point in Movement Allowance.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_DecreaseMovement.png"},
      {"name": "Smashed hip", "effect": "DEC_MA", "info": "Loses 1 point in Movement Allowance.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_DecreaseMovement.png"},
      {"name": "Fractured skull", "effect": "DEC_AV", "info": "Loses 1 point in Armour Value.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_DecreaseArmour.png"},
      {"name": "Serious Concussion", "effect": "DEC_AV", "info": "Loses 1 point in Armour Value.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_DecreaseArmour.png"},
      {"name": "Broken neck", "effect": "DEC_AG", "info": "Loses 1 point in Agility.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_DecreaseAgility.png"},
      {"name": "Smashed collar bone", "effect": "DEC_ST", "info": "Loses 1 point in Strength.", "img": "https://spike.ovh/static/spike_resources/img/injuries/Skill_DecreaseStrength.png"}
    ]

    for case in casualties:
        injuries_ids.append(injuries_db.add_injury(case))
    return injuries_ids


def generate_inducements():
    inducements_db = TCInducements()
    inducements_ids = []
    inducements = [
        {"name": "Bloodweiser babe", "min": 0, "max": 2, "price": 50, "img": "", "info": ""},
        {"name": "Bribe", "min": 0, "max": 3, "price": 100, "img": "", "info": "Default Bribe"},
        {"name": "Extra team training", "min": 0, "max": 3, "price": 100, "img": "", "info": ""},
        {"name": "Halfling master chef", "min": 0, "max": 1, "price": 300, "img": "", "info": "Default halfing master chef"},
        {"name": "Apothecary", "min": 0, "max": 2, "price": 100, "img": "", "info": ""},
        {"name": "Wizard", "min": 0, "max": 1, "price": 150, "img": "", "info": ""}
    ]

    for inducement in inducements:
        inducements_ids.append(inducements_db.add_inducement(inducement))

    goblin_bribe = {"name": "Bribe", "min": 0, "max": 3, "price": 50, "img": "", "info": "Goblin bribe"}
    halfling_chef = {"name": "Halfling master chef", "min": 0, "max": 1, "price": 100, "img": "", "info": "Default halfing master chef"}
    inducements_db.add_inducement(goblin_bribe)
    inducements_db.add_inducement(halfling_chef)
    return inducements_ids


def generate_blood_bowl_rule(skill_ids, injuries_id, races_ids):
    rules_db = TCRules()
    europen_rules = read_json("europen_rules.json")
    europen_rules["races"] = list(races_ids.values())
    europen_rules["injuries"] = injuries_id
    europen_rules["skills"] = []
    for key, value in skill_ids.items():
        europen_rules["skills"].append(value)
    rules_db.add_rule(europen_rules)

    bb2_rules = read_json("bb2_rules.json")
    bb2_rules["races"] = list(races_ids.values())
    bb2_rules["injuries"] = injuries_id
    bb2_rules["skills"] = []
    for key, value in skill_ids.items():
        bb2_rules["skills"].append(value)
    rules_db.add_rule(bb2_rules)


def main():
    skill_ids = generate_skills()
    players_id = generate_players(skill_ids)
    star_players_ids = generate_star_players(skill_ids)
    injuries_id = generate_injuries()
    inducements_ids = generate_inducements()
    races_ids = generate_races(players_id, star_players_ids, inducements_ids)

    generate_blood_bowl_rule(skill_ids, injuries_id, races_ids)

    sys.exit()


if __name__ == "__main__":
    main()