FROM python:3

COPY . /app

# Set the working directory
WORKDIR /app

# Install the dependencies
RUN pip install pymongo

CMD [ "python", "./main.py" ]